<?php

function sacar(array $conta, float $valorASacar): array
{
    if ($valorASacar > $conta['saldo']) {
        showMessage("Você não pode sacar: ");
    } else {
        $conta['saldo'] -= $valorASacar;
    }
    return $conta;
}


function showMessage(string $message): void
{
    echo $message . PHP_EOL;
}

function depositar(array $conta, float $valorADepositar): array
{
    if ($valorADepositar > 0) {
        $conta['saldo'] += $valorADepositar;
    } else {
        showMessage("Depositos precisam ser positivos: ");
    }
    
    return $conta;
}

function upperCase(array &$conta)
{
    $conta['titular'] = mb_strtoupper($conta['titular']);
}
<?php

require_once 'functions.php';

$contasCorrentes = [
    '123.456.789-10' => [
        'titular' => 'Vinicius',
        'saldo' => 1000
    ],
    '987.456.321-00' => [
        'titular' => 'nelynely',
        'saldo' => 4000
    ],
    '123.456.789-11' => [
        'titular' => 'Brian',
        'saldo' => 200
    ]
];

$contasCorrentes['123.456.789-10'] = sacar($contasCorrentes['123.456.789-10'], 500);
$contasCorrentes['123.456.789-11'] = depositar($contasCorrentes['123.456.789-11'], 800);

unset($contasCorrentes['123.456.789-10']);

upperCase($contasCorrentes['123.456.789-11']);

foreach ($contasCorrentes as $cpf => $conta) {
    ['titular' => $titular, 'saldo' => $saldo] = $conta;
    showMessage("$cpf $titular $saldo");
}



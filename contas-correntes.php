<?php


$contasCorrentes = [
    '123.456.789-10' =>  [
        'titular' => 'Vinicius',
        'saldo' => 1000
    ], 
    '987.456.321-00' => [
        'titular' => 'nelynely',
        'saldo' => 4000
    ], 
    '123.456.789-11' => [
        'titular' => 'Brian',
        'saldo' => 200
    ]
];

$contasCorrentes['369.258.741-00'] = [
    'titular' => 'Amy',
    'saldo' => 2000
];

foreach ($contasCorrentes as $cpf => $conta) {
    echo $cpf . PHP_EOL;
}
    

